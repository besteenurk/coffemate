package com.example.bestenurkaradeniz.coffeemate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    Users beste = new Users("Beste",123456);
    Users selin = new Users("Selin",654321);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText username = findViewById(R.id.username);
        final EditText password = findViewById(R.id.password);
        final TextView exp = findViewById(R.id.exp);
        Button login = findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intet = new Intent(getApplication(), CoffeeList.class);

                if ((!username.getText().toString().equals(beste.getUsername()) && !password.getText().toString().equals(beste.getPassword()))) {
                    exp.setText("TRY AGAIN");
                } else {
                    startActivity(intet);
                }

            }
        });


    }
}
